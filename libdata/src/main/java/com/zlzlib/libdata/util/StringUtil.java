package com.zlzlib.libdata.util;

import android.text.Editable;
import android.text.TextUtils;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * 字符串转化的工具
 *
 * @DateTime: 2020/4/15 10:58
 * @Author zlz
 * @Version 1.0
 */
public class StringUtil {

    public static int toInt(String num) {
        int i = 0;
        try {
            i = Integer.parseInt(num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }

    public static long toLong(String num) {
        long i = 0;
        try {
            i = Long.parseLong(num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }

    public static double toDouble(String num) {
        double i = 0.0;
        try {
            i = Double.parseDouble(num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }

    public static float toFloat(String num) {
        float i = 0.0f;
        try {
            i = Float.parseFloat(num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }

    /**
     * @param text
     * @return 获得TextView 的内容值
     */
    public static String getTextValue(TextView text) {
        return TextUtils.isEmpty(text.getText()) ? "" : text.getText().toString();
    }

    /**
     * 判断去除空格后是不是为空
     *
     * @param text
     * @return true 空  false 不为空
     */
    public static boolean isTextEmptyNoSpace(Editable text) {
        return TextUtils.isEmpty(text) || text.toString().replace(" ", "").isEmpty();
    }

    private final static Pattern CHINESE = Pattern
            .compile("[\\u4e00-\\u9fa5]+");

    private final static Pattern NUMBERS = Pattern
            .compile("[0-9]+");

    private final static Pattern LETTERS = Pattern
            .compile("[a-zA-z]+");

    /**
     * 判断是否是中文（一个以上）
     *
     * @param str 中文字符串
     * @return true:是中文,false:非中文
     */
    public static boolean isChinese(String str) {
        return CHINESE.matcher(str).matches();

    }

    /**
     * 判断输入是否为字母（一个以上）
     *
     * @param str 字符串
     * @return true:是字母,false:其它
     */
    public static boolean isLetters(String str) {
        return LETTERS.matcher(str).matches();

    }

    /**
     * 判断是否为数字（一个以上）
     *
     * @param str 字符串
     * @return true:是数字,false:其它
     */
    public static boolean isNumbers(String str) {
        return NUMBERS.matcher(str).matches();
    }

    private static final String EMPTY_STRING = "";

    public static String join(String[] array) {
        if (array == null) {
            return EMPTY_STRING;
        }
        return join(Arrays.asList(array));
    }


    public static void join(String[] array, char separator, StringBuilder sb) {
        if (array == null) {
            return;
        }
        join(Arrays.asList(array), separator, sb);
    }


    public static String join(Collection<String> collection) {
        return join(collection, ',');
    }


    public static String join(Collection<String> collection, char separator) {
        // Shortcut
        if (collection == null || collection.isEmpty()) {
            return EMPTY_STRING;
        }

        StringBuilder result = new StringBuilder();
        join(collection, separator, result);
        return result.toString();
    }

    public static void join(Iterable<String> iterable, char separator, StringBuilder sb) {
        if (iterable == null) {
            return;
        }
        boolean first = true;
        for (String value : iterable) {
            if (first) {
                first = false;
            } else {
                sb.append(separator);
            }
            sb.append(value);
        }
    }

}
