package com.zlzlib.libdata.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 一些日期时间的工具方法
 *
 * @DateTime: 2020/7/20 14:49
 * @Author zlz
 * @Version 1.0
 */
public class MyDateTimeUtil {

    /**
     * @return 获得当前年份
     */
    public static String getNowYear() {
        return getFormDateTime(System.currentTimeMillis(), "yyyy");
    }

    /**
     * @return 获得当前月份
     */
    public static String getNowMoth() {
        return getFormDateTime(System.currentTimeMillis(), "MM");
    }

    /**
     * @return 获得当前日期
     */
    public static String getToday() {
        return getFormDateTime(System.currentTimeMillis(), "dd");
    }

    /**
     * @return 获得当前几点
     */
    public static String getNowHour() {
        return getFormDateTime(System.currentTimeMillis(), "HH");
    }

    /**
     * @return yyyy-MM-dd
     */
    public static String getCurrentDate() {
        return getCurrentDate("yyyy-MM-dd");
    }

    /**
     * @param format 格式
     * @return 获取当前日期
     */
    public static String getCurrentDate(String format) {
        return getFormDateTime(System.currentTimeMillis(), format);
    }

    /**
     * 获取当前传入天数前后几天的日期
     *
     * @param startDate 原始日期
     * @param rangeDays 差距天数  负数为前几天  正数为后几天
     * @return 目标日期
     */
    public static Date getRangeDate(Date startDate, int rangeDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.DATE, rangeDays);
        return calendar.getTime();
    }

    /**
     * @param time 获得的时间
     * @return 获得一个日期 格式 yyyy-MM-dd HH:mm:ss
     */
    public static String getNormalTime(Long time) {
        return getFormDateTime(time, "yyyy-MM-dd HH:mm:ss");
    }


    /**
     * @param format 格式
     * @return 获得日期格式转化器  中国时区的
     */
    public static SimpleDateFormat getMyDateFormat(String format) {
        SimpleDateFormat df = new SimpleDateFormat(format, Locale.getDefault());
        //设置时区为中国时区
        df.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
        return df;
    }

    /**
     * @param time    获得的时间
     * @param pattern 格式  如  yyyy-MM-dd HH:mm:ss
     * @return 格式日期
     */
    public static String getFormDateTime(Long time, String pattern) {
        Date date = new Date(time);
        return getMyDateFormat(pattern).format(date);
    }

    /**
     * @param dateStr 日期
     * @param format  格式
     * @return 把用户输入的日期转成 java 日期类
     */
    public static Date convertString2Date(String dateStr, String format) {
        DateFormat df = getMyDateFormat(format);
        Date date = new Date();
        try {
            date = df.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * @param dateStr 日期
     * @return 把用户输入的yyyy-MM-dd日期 转成 java 日期类
     */
    public static Date convertString2Date_yyyyMMdd(String dateStr) {
        return convertString2Date(dateStr, "yyyy-MM-dd");
    }

    /**
     * 获取两个日期之间的间隔天数
     *
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @return 日期相隔天数
     */
    public static int getGapDaysCount(Date startDate, Date endDate) {
        try {
            //去除时分秒的数据
            Calendar fromCalendar = Calendar.getInstance();
            fromCalendar.setTime(startDate);
            fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
            fromCalendar.set(Calendar.MINUTE, 0);
            fromCalendar.set(Calendar.SECOND, 0);
            fromCalendar.set(Calendar.MILLISECOND, 0);
            Calendar toCalendar = Calendar.getInstance();
            toCalendar.setTime(endDate);
            toCalendar.set(Calendar.HOUR_OF_DAY, 0);
            toCalendar.set(Calendar.MINUTE, 0);
            toCalendar.set(Calendar.SECOND, 0);
            toCalendar.set(Calendar.MILLISECOND, 0);
            return (int) ((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static final String[] weekData1 = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
    private static final String[] weekData2 = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};

    public static String getWeek(int type) {
        return getWeek("", type);
    }

    /**
     * @param date 需要转化的日期
     * @param type 文字类型 0==星期 1==周
     * @return 星期几
     */
    public static String getWeek(String date, int type) {
        if (null == date || "".equals(date)) {
            date = getCurrentDate();
        }
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(getMyDateFormat("yyyy-MM-dd").parse(date));
        } catch (Exception e) {
            return "";
        }
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) {
            w = 0;
        }
        if (type == 0) {
            return weekData1[w];
        }
        return weekData2[w];
    }
}
