package com.zlzlib.libdata.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;


import com.zlzlib.libdata.BuildConfig;

import java.util.Locale;

/**
 * Created by zlz on 2017/10/26.
 */

public class LogUtil {

    private static boolean isLogEnabled = BuildConfig.DEBUG;// 默认开启
    private static String defaultTag = "LogUtil";// log默认的 tag
    private static final String TAG_CONTENT_PRINT = "%s.%s:%d";

    /**
     * @return 获得当前的 堆栈
     */
    private static StackTraceElement getCurrentStackTraceElement() {
        return Thread.currentThread().getStackTrace()[4];
    }

    /**
     * 设置 debug是否启用 根据 判断 是否 为上线模式 android:debuggable 打包后变为false，没打包前为true
     * <p/>
     * 要在application中 首先进行调用此方法 对 isLogEnabled 进行赋值
     */
    public static void setDebugAble(Context context) {
        try {
            ApplicationInfo info = context.getApplicationInfo();
            isLogEnabled = (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setDebugAble(boolean debugAble) {
        isLogEnabled = debugAble;
    }

    public static void setDefaultTag(String defaultTag) {
        LogUtil.defaultTag = defaultTag;
    }

    /**
     * @return 获取是否DEBUG模式
     */
    public static boolean isDebugAble() {
        return isLogEnabled;
    }

    /**
     * @return 打印的log信息 类名.方法名:行数--->msg
     */
    private static String getContent(StackTraceElement trace) {
        return String.format(Locale.CHINA, TAG_CONTENT_PRINT, trace.getClassName(), trace.getMethodName(),
                trace.getLineNumber());
    }

    /**
     * debug
     *
     * @param tag
     * @param msg
     */
    public static void d(String tag, String msg, Throwable tr) {
        if (isLogEnabled) {
            Log.d(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg, tr);
        }
    }

    /**
     * debug
     *
     * @param tag
     * @param msg
     */
    public static void d(String tag, String msg) {
        if (isLogEnabled) {
            // getContent(getCurrentStackTraceElement())
            Log.d(tag, "--->" + msg);
        }
    }

    /**
     * debug
     *
     * @param msg
     */
    public static void d(String msg) {
        if (isLogEnabled) {
            Log.d(defaultTag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

    /**
     * error
     *
     * @param tag
     * @param msg
     */
    public static void e(String tag, String msg, Throwable tr) {
        if (isLogEnabled) {
            Log.e(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg, tr);
        }
    }

    /**
     * error
     *
     * @param tag
     * @param msg
     */
    public static void e(String tag, String msg) {
        if (isLogEnabled) {
            Log.e(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

    /**
     * error
     *
     * @param msg
     */
    public static void e(String msg) {
        if (isLogEnabled) {
            Log.e(defaultTag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

    /**
     * info
     *
     * @param tag
     * @param msg
     */
    public static void i(String tag, String msg, Throwable tr) {
        if (isLogEnabled) {
            Log.i(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg, tr);
        }
    }

    /**
     * info
     *
     * @param tag
     * @param msg
     */
    public static void i(String tag, String msg) {
        if (isLogEnabled) {
            Log.i(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

    /**
     * info
     *
     * @param msg
     */
    public static void i(String msg) {
        if (isLogEnabled) {
            Log.i(defaultTag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

    /**
     * verbose
     *
     * @param tag
     * @param msg
     */
    public static void v(String tag, String msg, Throwable tr) {
        if (isLogEnabled) {
            Log.v(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg, tr);
        }
    }

    /**
     * verbose
     *
     * @param tag
     * @param msg
     */
    public static void v(String tag, String msg) {
        if (isLogEnabled) {
            Log.v(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

    /**
     * verbose
     *
     * @param msg
     */
    public static void v(String msg) {
        if (isLogEnabled) {
            Log.v(defaultTag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

    /**
     * warn
     *
     * @param tag
     * @param msg
     */
    public static void w(String tag, String msg, Throwable tr) {
        if (isLogEnabled) {
            Log.w(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg, tr);
        }
    }

    /**
     * warn
     *
     * @param tag
     * @param msg
     */
    public static void w(String tag, String msg) {
        if (isLogEnabled) {
            Log.w(tag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

    /**
     * warn
     *
     * @param msg
     */
    public static void w(String msg) {
        if (isLogEnabled) {
            Log.w(defaultTag, getContent(getCurrentStackTraceElement()) + "--->" + msg);
        }
    }

}
