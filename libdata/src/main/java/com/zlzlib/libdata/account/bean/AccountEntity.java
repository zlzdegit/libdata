package com.zlzlib.libdata.account.bean;

import java.io.Serializable;

/**
 * 用户实体
 *
 * @DateTime: 2020/2/5 11:24
 * @Author zlz
 * @Version 1.0
 */
public class AccountEntity implements Serializable {

    private String account;
    private String password;

    public AccountEntity() {
    }

    public AccountEntity(String account, String password) {
        super();
        this.account = account;
        this.password = password;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
