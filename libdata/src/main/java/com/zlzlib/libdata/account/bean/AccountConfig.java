package com.zlzlib.libdata.account.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 用户数据配置
 *
 * @DateTime: 2020/2/5 11:23
 * @Author zlz
 * @Version 1.0
 */
public class AccountConfig implements Serializable {

    private boolean remember = false;

    private List<AccountEntity> list;

    public AccountConfig() {
        list = new LinkedList<>();
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

    public List<AccountEntity> getList() {
        return list;
    }

    public void setList(List<AccountEntity> list) {
        this.list = list;
    }

    public void add(String account, String password) {
        list.add(new AccountEntity(account, password));
    }

    public void remove(String account) {
        List<AccountEntity> del = new ArrayList<>();
        for (AccountEntity accountEntity : list) {
            if (account.equals(accountEntity.getAccount())) {
                del.add(accountEntity);
            }
        }
        list.removeAll(del);
    }

}
