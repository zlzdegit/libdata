package com.zlzlib.libdata.account;


import com.zlzlib.libdata.account.bean.AccountEntity;

/**
 * 用户登录账户密码记录器
 *
 * @DateTime: 2020/2/5 11:22
 * @Author zlz
 * @Version 1.0
 */
public interface AccountService {

    void setSavePath(String path, String fileName);

    /**
     * 记录用户信息
     *
     * @param account  账号
     * @param password 密码
     */
    void add(String account, String password);

    /**
     * @param account 移除当前账号
     */
    void remove(String account);

    /**
     * @return 取出所有用户名称
     */
    String[] getAllAccount();

    /*
     * 获取用户密码
     * @param account
     */
    String getPassword(String account);

    /**
     * 保存用户信息
     */
    void save();

    /**
     * 读取用户信息
     */
    void read();

    /**
     * @return 是否记住密码
     */
    boolean IsRemember();

    /*
     * 是否记住密码
     */
    void setRemember(boolean isRemember);

    /**
     * @return 当前用户
     */
    AccountEntity getCurrentAccount();

}
