package com.zlzlib.libdata.account.impl;


import com.zlzlib.libdata.account.AccountService;
import com.zlzlib.libdata.account.bean.AccountConfig;
import com.zlzlib.libdata.account.bean.AccountEntity;
import com.zlzlib.libdata.util.MyFileUtil;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * 用户登录账户密码记录器 数据操作实现类
 *
 * @DateTime: 2020/2/5 11:25
 * @Author zlz
 * @Version 1.0
 */
public class AccountServiceFactory implements AccountService {

    private volatile static AccountService instance = null;

    public static AccountService getInstance() {
        if (instance == null) {
            synchronized (AccountServiceFactory.class) {
                if (instance == null) {
                    instance = new AccountServiceFactory();
                }
            }
        }
        return instance;
    }

    private AccountServiceFactory() {
        config = new AccountConfig();
    }

    private String savePath = "";

    private AccountConfig config;

    /**
     * 当前用户
     */
    private AccountEntity curAccount;

    @Override
    public void setSavePath(String savePath, String fileName) {
        MyFileUtil.createNewDri(savePath);
        this.savePath = MyFileUtil.createNewFile(savePath, fileName).getAbsolutePath();
    }

    @Override
    public void add(String account, String password) {
        if (config.isRemember()) {
            config.add(account, password);
        }
        curAccount = new AccountEntity(account, password);
    }

    @Override
    public void remove(String account) {
        config.remove(account);
    }

    @Override
    public String[] getAllAccount() {
        List<String> list = new LinkedList<>();
        for (AccountEntity element : config.getList()) {
            list.add(element.getAccount());
        }
        return list.toArray(new String[]{});
    }

    @Override
    public String getPassword(String account) {
        String password = "";
        for (AccountEntity element : config.getList()) {
            if (account.equals(element.getAccount())) {
                password = element.getPassword();
                break;
            }
        }
        return password;
    }

    @Override
    public void save() {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(savePath);
            ObjectOutputStream objectInputStream = new ObjectOutputStream(fileOutputStream);
            objectInputStream.writeObject(config);
            objectInputStream.flush();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void read() {
        try {
            FileInputStream fileInputStream = new FileInputStream(savePath);
            if (fileInputStream.available() > 0) {
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                config = (AccountConfig) objectInputStream.readObject();
                objectInputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean IsRemember() {
        return config.isRemember();
    }

    @Override
    public void setRemember(boolean isRemember) {
        config.setRemember(isRemember);
    }

    @Override
    public AccountEntity getCurrentAccount() {
        return curAccount;
    }
}
