package com.zlzlib.libdata.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by zlz on 2018/7/13.
 * SharedPreferences 的基本类  存储键值对数据
 */
public abstract class BaseShareHelper {

    private final SharedPreferences share;

    protected abstract String shareName();

    protected int shareMode() {
        return Context.MODE_PRIVATE;
    }

    protected BaseShareHelper(Context context) {
        share = context.getSharedPreferences(shareName(), shareMode());
    }

    protected void setString(String key, String value) {
        share.edit().putString(key, value).apply();
    }

    protected String getString(String key, String defaultValue) {
        return share.getString(key, defaultValue);
    }

    protected String getString(String key) {
        return share.getString(key, "");
    }

    protected void setBoolean(String key, boolean value) {
        share.edit().putBoolean(key, value).apply();
    }

    protected boolean getBoolean(String key, boolean defaultValue) {
        return share.getBoolean(key, defaultValue);
    }

    protected void setInt(String key, int value) {
        share.edit().putInt(key, value).apply();
    }

    protected int getInt(String key, int defaultValue) {
        return share.getInt(key, defaultValue);
    }

    protected void setLong(String key, long value) {
        share.edit().putLong(key, value).apply();
    }

    protected long getLong(String key, long defaultValue) {
        return share.getLong(key, defaultValue);
    }

    protected void apply() {
        share.edit().apply();
    }

    protected BaseShareHelper addString(String key, String value) {
        share.edit().putString(key, value);
        return this;
    }

    protected BaseShareHelper addBoolean(String key, boolean value) {
        share.edit().putBoolean(key, value);
        return this;
    }

    protected BaseShareHelper addInt(String key, int value) {
        share.edit().putInt(key, value);
        return this;
    }

    protected BaseShareHelper addLong(String key, long value) {
        share.edit().putLong(key, value);
        return this;
    }
}
